# Security Fundamentals Notes

Security Fundamentals 41900 - Final exam notes

## Getting Started

Simply click on the individual files on the repo. They are markdown files so it should be all pretty and formatted.

The repo can also be cloned and `.md` files viewed using your favourite text editor:

    $ git clone https://gitlab.com/drsh0/secfund-41900.git

## Built With

* Atom + Markdown Plus plugin
* Gitlab

## Contributing

I encourage all pull requests and corrections.

<!---
## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
-->
