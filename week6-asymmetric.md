<!-- TOC START min:1 max:3 link:true update:true -->
- [Week 6 - Asymmetric key cryptography & RSA](#week-6---asymmetric-key-cryptography--rsa)
  - [Public Key Cryptography](#public-key-cryptography)
    - [Asymmetric cipher](#asymmetric-cipher)
    - [Why Asymmetric key?](#why-asymmetric-key)
  - [Diffie-Hellman Key Exchange](#diffie-hellman-key-exchange)
    - [Attacks](#attacks)
  - [RSA](#rsa-1)
  - [Symmetrics vs Asymmetric](#symmetrics-vs-asymmetric)
    - [Symmetric](#symmetric)
    - [Asymmetric](#asymmetric)

<!-- TOC END -->

# Week 6 - Asymmetric key cryptography & RSA

- Symmetric key cryptography = sharing secrecy
- Asymmetric key cryptography = personal secrecy

## Public Key Cryptography
* Uses a secret **private** key and a published **public** key (both mathematically related)
* Private key cannot be derived from the public key

### Asymmetric cipher

<img src="assets/wk6-1.png" width="500">

#### RSA
* Public and private keys are interchangeable
* Variable key size = 512, 1024, 2048 bits
* Most popular

#### EIGamal
* 512 or 1024 bits key
* Used with PGP

### Why Asymmetric key?
* Start secure communication when there is no way to exchange secrets beforehand
* PKI used to certify keys

## Diffie-Hellman Key Exchange
As discussed earlier using simplified terms, DH key exchange uses mathematical tricks. The trick used is detailed below:
<!-- TODO -->
1. Alice and Bob agree on a large prime *p* and a generator *g*

| Strengths of DH |
-----------
|Hard to solve discrete log problem|
|difficult to calculate DH problem|
|small generator req.|
|shared secret colour is not used as a key|


### Attacks
* Computing the initial calculations during DH is quite easy
* However, there are 2 things that are hard: `Discrete Log Problem` and `DH Problem`
* The above refer to reversals of the mathematical tricks used to create a shared secret. As of today, this has not been solved but it can be in the future
* `Discrete Log Problem`:
  * Exhaustive search
* There are **RFC Standards** that define how *p* and *g* is chosen. Therefore, all that is needed is to state the **RFC** standard no for both parties to start the key exchange.

## RSA
<!-- Video that may help: https://www.youtube.com/watch?v=4zahvcJ9glg -->
* Used for public key encryption


## Symmetrics vs Asymmetric

### Symmetric
| Advantages | Disadvantages |
-------------|----------------
|Short key length| Keys have to remain secret both ways|
|Can be used to initiate PRNG| For multi parties you need `n(n-1)/2` keys|
|Exhaustive key search is only attach|
|Better for thoroughput|

### Asymmetric
| Advantages | Disadvantages |
-------------|----------------
|only private key needs to be kept safe|Slower than Symmetric|
|Long lifespan (*Moore's law*)|large key sizes|
||Less developed compared to symmetric keys
