<!-- TOC START min:1 max:3 link:true update:true -->
- [Week 5 - AES & Key Management](#week-5---aes--key-management)
    - [Block Ciphers](#block-ciphers)
    - [DES - Data Encryption Standard](#des---data-encryption-standard)
    - [AES - Advanced Encryption Standard](#aes---advanced-encryption-standard)
    - [Key Distribution](#key-distribution)

<!-- TOC END -->

# Week 5 - AES & Key Management

### Block Ciphers

* Plaintext is encrypted in blocks. The symmetric key is then used to reverse the encrypted block/s
* The blocks are mapped in a pre-determined way.

<img src="assets/des1.png" width="300">

* To break a perfect block cipher, brute force is the only option as even knowing bits of input/output will not yield any information until all permutation paths are tried.
    - Normally, this will take a very long time with modern block ciphers using 128 bit keys (2^128) requiring all possible 128! attempts.

### DES - Data Encryption Standard
* Created in 1977; 64 bit key but uses 56 bits for key (8 bits for parity checks)
* Easily brute forced in 2^55 operations

#### 3DES - 2-key Triple DES
* DES 3 times with 2 (or 3) keys - DES algo applied 3 times while also expanding the number of key bits. Still not practical to use as modern hardware can attack the key/s

#### DESX
* An additional key `k2` with a value of 64 bits is used and the third key is a hash function (also known as a *whitening key*)

### AES - Advanced Encryption Standard
* 128 bit blocks
* 128/192/256 bit keys
* Competition for this block cipher won by **Rijndael** in Oct 2000

| Rounds | key length |
|---|---|
|10|128 bit|
|12   |192 bit |
|14   |256 bit   |

### Key Distribution

* **Key establishment** - Shared key becomes available to 2 or more parties
* **Key management** - Key agreement, transport, replacement, maintain relationships between parties

#### Naive key distribution
- A key distribution centre can be centralised and offer key pairs to parties. However it results in
    - single point of failure
    - not very scalable
    - no authentication
    - slow

#### Merkle's Puzzles
- Key exchange without requiring 3rd party
- Bob creates `N` puzzles
- Bob sends all puzzles to Alice
- Alice chooses a random puzzle
    - brute forces and finds correct puzzle - *key space search*

#### Diffie-Hellman Key Exchange
- Key exchange method devised in 1976 that uses mathematical tricks to ensure key exchange is kept secret.
- **Illustration**

    1. Alice and Bob both agree on a **base colour**
    2. Alice and Bob both choose their own **private colours**
    3. Alice and Bob mix the base colour with their own **private colours**
    4. Each party's **private colour** is sent to each other publically
    5. Each party combines their **private colour** with the **mixed colour** of the other party. This creates a truly unique **secret colour** that is same for both parties
    6. This is the the **shared secret** that both parties can rely on without anyone else reversing the order

- Video:

    <a href="http://www.youtube.com/watch?feature=player_embedded&v=YEBfamv-_do"><img src="http://img.youtube.com/vi/YEBfamv-_do/0.jpg"
    alt="IMAGE ALT TEXT HERE" width="240" height="180"/></a>
