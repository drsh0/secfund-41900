<!-- TOC START min:1 max:3 link:true update:true -->
- [Digital Signature](#digital-signature)
  - [Properties](#properties)
    - [Structure](#structure)
  - [Public Key Signatures](#public-key-signatures)
  - [Signature Attacks](#signature-attacks)
    - [RSA Signatures](#rsa-signatures)
- [SSL/TLS](#ssltls)
    - [TLS](#tls)
    - [Certificates](#certificates)

<!-- TOC END -->



# Digital Signature

## Properties

- **Authentic** - we should believe signer signed the document
- **Unforgeable** - Only signer can sign the document
- **Non-reusable** - Signature can't be reused on another document
- **Unalterable** - Cannot be changed after signing
- **Non-repudiation** - The signer cannot deny they signed it at a later time

### Structure

    S = F(m, k)

Message signed with key `k` (private) which assigns signature `S` to message `m` through scheme `F`. `(m, S)` is enough to verify sig.

## Public Key Signatures

1. Adam creates keys:

    a. one for signing (private)

    b. one for verifying (public)
2. Adam's public key is published

3. Adam creates signature:

    a. Chose random bits

    b. hash message and get message digest

    c. generates `S = S(digest, rand, private key)`.

    d. send `S` and `m` to Alice

4. Alice verifies sig

    a. Alice gets public key from directory

    b. calculate `d` with `hashf(m)`.

    c. `verify(d, public key, signature)`

## Signature Attacks

* Total break - Attacker can find private from public key
* Selective forgery - Some messages can be forged
* Existential forgery - theory only
* Signature replay:
    - to maintain the message the signature belongs to is fresh a **nonce** is used (random no)
    - the nonce is kept track of to avoid messages being replayed

### RSA Signatures

`<TODO>`



# SSL/TLS

* HTTP = plaintext
* tampering, eavesdropping, MITM
* SSL = Secure Socket Layer
* TLS = Transport Layer Security
* Both use asymmetric crypto = auth and symmetric crypto for message + MACs for integrity

### TLS

1. Client --> Server:

    a. SSL version

    b. My available ciphers + other info
2. Server --> Client:

    a. SSL version

    b. I want this cipher

    c. Here is the cert.
3. Client authenticates server by signing with cert.
4. Client creates secret for session, encrypts with cert and sends to server
5. (opt) Server may authenticate client with client cert

### Certificates

* **Certificate Authority** = trusted 3rd party; shipped with OS/vendors/software
* A server's certificate is checked against the CA
    - Not all CAs are trustworthy or secure
    - Fraudulent certs possible for MITM attacks
    - Validation methods don't prove you own domain (DNS TXT or admin@site.com)
    - **extended validation** available with ID
* CA checks the CSR you send (with public key). Once confirmed, CA signs cert and sends it to owner which is then installed on server.
* Certificates can be revoked using **Online Cert Status Protocol** (OCSP).
