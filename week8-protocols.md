# Security Protocols

## Problems with Diffie-Hellman

### Man-in-the-middle
![snoop](/assets/protocols-1.png)

* Eve owns the channel
* The other's don't know that
* Secrets are not safe any more

#### Definitions
* **Perfect Forward Secrecy** - if disclosure of future keys does not give up past sessions
    - Automatically provided by Diffie-Hellman
    - Not with RSA public keys
* **Known-key attack** - Finding past keys allows adversary to compromise future sessions

#### Needham-Schroeder Protocol
- key exchange using a trusted third party = *Trent*
- Alice sends request to Trent with a nonce random alue
- Trent sends a **session key**, and a ticket to give to Bob
- Alice sends ticket to Bob
- Bob challenges Alice with nonce, Alice responds

##### Problems
1. Obtain key that Alice and Trent share
2. Results in impersonation and snooping
3. Key can't be revoked unless Trent does so

#### Kerberos / CA
- Utilises CA and certs to ensure TPP are good guys
- CA signs certs based on ther person's public key. Unlocked only with private key
- If CA is compromised, people can be imposters but priv. key not exposed
- Certs can be revoked if private key compromised

#### Trust Models
- Symmetric keys: TTP used online. TTP huge target as they know keys **no forward secrecy**
- Asymmetric keys: TTP offline. Only knows public keys == **forward secrecy**

## Secret Splitting and Sharing
